import { Dart, User } from '.';

export interface Player extends User {
    active: boolean,
    darts: Dart[],
    score: number,
    finish: string,
}
