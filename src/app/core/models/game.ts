import { Player } from '.';

export enum Mode {
    // Important: NEVER change order of enum values, will lead to problems with games stored in DB
    X01,
    CRICKET,
}

export interface Game {
    mode: Mode;
    score: number;
    useDoubleIn: boolean;
    useDoubleOut: boolean;
    useMasterOut: boolean;
    start: Date;
    end: Date;
    players: (Player | null)[];
}
