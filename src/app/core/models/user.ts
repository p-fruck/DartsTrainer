export enum Usertype {
    GUEST = 'guest',
    LOCAL = 'local',
    ONLINE = 'online',
}

export interface User {
    id?: number,
    name: string,
    type: Usertype,
}
