import { Injectable } from '@angular/core';
import { Gamemode } from '@app/core/gamemodes';
import { IndexedDbService } from '.';

@Injectable({
  providedIn: 'root',
})

export class GameService {
  private gamemode: Gamemode | undefined;

  constructor(private dbService: IndexedDbService) { }

  startGame(gamemode: Gamemode) {
    this.gamemode = gamemode;
  }

  get game(): Gamemode | undefined {
    return this.gamemode;
  }

  get initialScore(): number {
    return this.gamemode?.initialScore || 0;
  }

  saveCurrentGame(): Promise<number> | undefined {
    if (!this.gamemode) return undefined;
    return this.dbService.addGame(this.gamemode.export());
  }
}
