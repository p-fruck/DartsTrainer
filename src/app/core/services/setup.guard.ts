import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot, CanActivate, Router,
} from '@angular/router';
import { routes } from '@env/config';
import { IndexedDbService } from '.';

@Injectable()
export class SetupGuard implements CanActivate {
  constructor(
    private indexedDbService: IndexedDbService,
    private router: Router,
  ) { }

  async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
    const isSetupWizard = route.toString().includes(routes.setup);

    // Browsers without IndexedDb support are allowed to join in "guest mode"
    const isAlreadySetup = this.indexedDbService
      .supportsIndexedDb ? await this.indexedDbService.hasUser() : true;

    if (isSetupWizard && isAlreadySetup) {
      this.router.navigate(['/', routes.home]);
    }
    if (!isSetupWizard && !isAlreadySetup) {
      this.router.navigate(['/', routes.setup]);
    }
    return isAlreadySetup !== isSetupWizard;
  }
}
