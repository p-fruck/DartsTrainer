import { Injectable } from '@angular/core';
import { User, Usertype } from '@app/core/models';
import { IndexedDbService } from '.';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private indexedDbService: IndexedDbService,
  ) { }

  async createUser(name: string): Promise<User|undefined> {
    const user: User = { name, type: Usertype.LOCAL };
    return this.indexedDbService.addUser(user);
  }

  async getUser(id = 1) {
    return this.indexedDbService.getUser(id);
  }

  async getUsers() {
    return this.indexedDbService.getUsers();
  }

  async getUserGamesCount(user: User) {
    return this.indexedDbService.getGamesCount(user.id || 0);
  }

  async updateUser(user: User) {
    return this.indexedDbService.users.update(user.id || 0, user);
  }

  async deleteUser(user: User) {
    return this.indexedDbService.users.delete(user.id || 0);
  }
}
