import Dexie from 'dexie';
import { Game, User } from '@app/core/models';

export class UserDatabase extends Dexie {
  users!: Dexie.Table<User, number>;

  games!: Dexie.Table<Game, number>;

  constructor() {
    // ToDo: Shared DB?
    super('DartsTrainer');
    this.version(1).stores({
      users: '++id,name,type',
      games: '++id,mode',
    });
  }
}
