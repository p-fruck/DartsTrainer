import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { routes as conf } from '@env/config';
import { GameGuard, SetupGuard } from '@app/core/services';
import { AboutComponent, SettingsComponent } from './components';

const routes: Routes = [
  {
    path: conf.bullOff,
    canActivate: [GameGuard, SetupGuard],
    loadChildren: () => import('./modules/bull-off/bull-off.module').then((m) => m.BullOffModule),
  },
  {
    path: conf.home,
    canActivate: [SetupGuard],
    loadChildren: () => import('./modules/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: conf.game,
    canActivate: [GameGuard, SetupGuard],
    loadChildren: () => import('./modules/game/game.module').then((m) => m.GameModule),
  },
  {
    path: conf.setup,
    canActivate: [SetupGuard],
    loadChildren: () => import('./modules/setup/setup.module').then((m) => m.SetupModule),
  },
  {
    path: conf.users,
    canActivate: [SetupGuard],
    loadChildren: () => import('./modules/users/users.module').then((m) => m.UsersModule),
  },
  {
    canActivate: [SetupGuard],
    path: conf.about,
    component: AboutComponent,
  },
  {
    canActivate: [SetupGuard],
    path: conf.settings,
    component: SettingsComponent,
  },

  // redirect to home by default
  { path: '**', redirectTo: conf.home, canActivate: [SetupGuard] },
];

// preload all components to save loading time
RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules });

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
