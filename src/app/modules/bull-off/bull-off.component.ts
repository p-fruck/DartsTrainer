import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Player } from '@app/core/models';
import { GameService } from '@app/core/services';
import { routes } from '@env/config';

@Component({
  selector: 'app-bull-off',
  templateUrl: './bull-off.component.html',
  styleUrls: ['./bull-off.component.sass'],
})
export class BullOffComponent implements OnInit {
  players: Player[];

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.players = gameService.game?.players || [];
  }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      if (params.rotate) this.rotate();
    });
  }

  /**
   * Called when a player gets dropped during a drag event
   * @param event - The drag event
   */
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.players, event.previousIndex, event.currentIndex);
  }

  /**
   * Useful if a revange is played. Puts the first player to the last position and the
   * other players one position up in order.
   */
  rotate() {
    const [player, ...players] = this.players;
    this.players = players.concat(player);
  }

  /**
   * Start a game with the current order of players
   */
  play() {
    this.gameService.game?.bullOff(this.players);
    this.router.navigate(['/', routes.game]);
  }
}
