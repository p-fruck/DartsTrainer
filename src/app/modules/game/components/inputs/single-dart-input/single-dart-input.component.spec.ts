import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleDartInputComponent } from './single-dart-input.component';

describe('SingleDartInputComponent', () => {
  let component: SingleDartInputComponent;
  let fixture: ComponentFixture<SingleDartInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SingleDartInputComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleDartInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
