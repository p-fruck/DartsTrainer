import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Gamemode } from '@app/core/gamemodes';
import { GameService } from '@app/core/services';
import { Player } from '@app/core/models';

interface DialogData { player: Player }

export type IPlayerWinsResult = 'exit' | 'restart' | 'continue';

@Component({
  selector: 'app-player-wins',
  templateUrl: './player-wins.component.html',
  styleUrls: ['./player-wins.component.sass'],
})
export class PlayerWinsComponent implements OnInit {
  game: Gamemode | undefined;

  players: Player[] = [];

  displayedColumns: string[] = ['position', 'name', 'average', 'darts'];

  constructor(
    public dialogRef: MatDialogRef<DialogData>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    gameService: GameService,
  ) {
    this.game = gameService.game;
  }

  ngOnInit(): void {
    if (!this.game) return;
    if (this.isGameOver) {
      this.players = this.game.rawPlayers;
    } else {
      this.players = this.game.rawPlayers.filter((player) => !player.active);
    }
  }

  /**
   * Returns true if all players have finished, false otherwise
   */
  get isGameOver() {
    return !this.game?.players?.length;
  }

  getAverage(player: Player): number {
    // calculate three dart average like the PDC
    if (!this.game) return 0;
    return ((this.game.initialScore - player.score) / player.darts.length) * 3;
  }

  exit(result: IPlayerWinsResult): void {
    this.dialogRef.close(result);
  }
}
