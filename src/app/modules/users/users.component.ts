import { AfterViewInit, Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { User } from '@app/core/models';
import { UserService } from '@app/core/services';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';

interface UserEntry extends User {
  gamesCount: number;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass'],
})
export class UsersComponent implements AfterViewInit {
  dataSource = new MatTableDataSource([] as UserEntry[]);

  displayedColumns: string[] = ['name', 'gamesCount', 'actions'];

  isLoadingResults = true;

  constructor(private dialog: MatDialog, private userService: UserService) { }

  private async updateDateSource() {
    this.isLoadingResults = true;
    const userEntries: UserEntry[] = [];
    const users = await this.userService.getUsers();
    await Promise.all(users
      .map((user) => this.userService.getUserGamesCount(user)
        .then((gamesCount) => userEntries.push({ ...user, gamesCount }))));
    this.dataSource = new MatTableDataSource(userEntries);
    this.isLoadingResults = false;
  }

  ngAfterViewInit(): void {
    this.updateDateSource();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  editUser(user: User) {
    const dialogRef = this.dialog.open(EditUserDialogComponent, {
      data: user,
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(async (updatedUser) => {
      if (updatedUser) {
        await this.userService.updateUser(updatedUser);
        await this.updateDateSource();
      }
    });
  }

  async deleteUser(user: User) {
    await this.userService.deleteUser(user);
    await this.updateDateSource();
  }
}
