import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { IndexedDbService } from '@app/core/services';
import { routes } from '@env/config';

interface IEntry {
  link: string,
  name: string,
  icon: string,
}

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.sass'],
})
export class NavigationComponent {
  toolbarTitle = '';

  private toolbarBLacklist = [
    `/${routes.setup}`,
    `/${routes.game}`,
  ];

  private toolbarNames: Record<string, string> = {};

  private initToolbarNames() {
    this.toolbarNames[`/${routes.settings}`] = 'Settings';
    this.toolbarNames[`/${routes.about}`] = 'About';
  }

  #entries: IEntry[] = [
    { link: `/${routes.home}`, name: 'Home', icon: 'home' },
    { link: `/${routes.users}`, name: 'Users', icon: 'people' },
    { link: `/${routes.settings}`, name: 'Settings', icon: 'settings' },
    { link: `/${routes.about}`, name: 'About', icon: 'contact_support' },
  ];

  constructor(router: Router, private indexedDbService: IndexedDbService) {
    this.initToolbarNames();
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd && event.url) {
        this.toolbarTitle = this.toolbarNames[window.location.pathname] ?? 'DartsTrainer';
      }
    });
  }

  private isValidEntry(entry: IEntry) {
    if (this.indexedDbService.supportsIndexedDb) return true;
    // hide links to pages that require indexedDb support
    return !['Users'].includes(entry.name);
  }

  get entries() {
    return this.#entries.filter(this.isValidEntry.bind(this));
  }

  get hideNavigation(): boolean {
    return this.toolbarBLacklist.includes(window.location.pathname);
  }

  dynamicButtonStyle() {
    return this.hideNavigation ? 'display: none' : 'color: white';
  }
}
